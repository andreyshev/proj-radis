package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	_ "github.com/lib/pq"

	"app/service"
)

func main() {
	log.Print("started")

	app, err := service.NewApp()
	if err != nil {
		log.Panic(err)
		return
	}

	r := mux.NewRouter()
	r.HandleFunc("/{key}", app.Load).Methods("GET")
	server := &http.Server{
		Addr:    ":9000",
		Handler: r,
	}

	go func() {
		err := server.ListenAndServe()
		if errors.Is(err, http.ErrServerClosed) {
			fmt.Printf("server closed\n")
		} else if err != nil {
			fmt.Printf("error listening for server one: %s\n", err)
		}
	}()
	waitKill()

	log.Print("stopped")
}

func waitKill() {
	waitKill := make(chan os.Signal, 1)

	signal.Notify(waitKill,
		os.Interrupt,
		syscall.SIGTERM,
		syscall.SIGINT,
		syscall.SIGSTOP,
		syscall.SIGKILL,
		syscall.SIGHUP,
		syscall.SIGQUIT,
	)

	<-waitKill
}
