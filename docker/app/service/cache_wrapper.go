package service

import (
	"context"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/redis/go-redis/v9"
)

type cacheWrapper struct {
	client *redis.Client
	db     *sqlx.DB
}

func newCacheWrapper() (*cacheWrapper, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "password",
		DB:       0,
	})

	if err := rdb.Ping(context.TODO()).Err(); err != nil {
		return nil, errors.Wrap(err, "redis ping")
	}
	db, err := dbConn()
	if err != nil {
		return nil, err
	}

	return &cacheWrapper{rdb, db}, nil
}

func (c *cacheWrapper) Close() error {
	c.client.Close()
	c.db.Close()
	return nil
}

func dbConn() (*sqlx.DB, error) {
	return sqlx.Open(os.Getenv("DB_DRIVER"), os.Getenv("DB_DSN"))
}

var (
	defaultDelta = 1000.0
	beta         = 1.0
	mu           = sync.Mutex{}
)

func (c *cacheWrapper) Get(key string, ttl time.Duration) error {
	delta, errGetKey := c.client.Get(context.TODO(), key).Float64()
	keyTtl := c.client.TTL(context.TODO(), key).Val()
	rand.Seed(time.Now().UnixNano())
	randomCoef := -(delta * beta * math.Log(rand.Float64()))
	needSetTtl := time.Duration(randomCoef * float64(time.Millisecond))
	if errGetKey != nil || delta == 0 || needSetTtl >= keyTtl {
		start := time.Now()
		if _, err := c.db.Exec("select $1::text", key); err != nil {
			return err
		}
		delta = float64(time.Now().Sub(start).Milliseconds())

		if err := c.Set(key, delta, ttl); err != nil {
			return err
		}
	}
	return nil
}

func (c *cacheWrapper) Set(key string, delta float64, ttl time.Duration) error {
	return c.client.Set(context.TODO(), key, delta, ttl).Err()
}
