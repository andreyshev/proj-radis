12. Redis

To run sentinel cluster

docker-compose up --scale redis-sentinel=3 -d

Test graphs
with wrapper:
![with-wrapper.png](test_results%2Fwith-wrapper.png)
without wrapper:
![without-wrapper.png](test_results%2Fwithout-wrapper.png)