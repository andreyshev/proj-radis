package service

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_newCacheWrapper(t *testing.T) {
	os.Setenv("DB_DRIVER", "postgres")
	os.Setenv("DB_DSN", "postgres://postgres:postgres@localhost:5432/?sslmode=disable")
	cache, err := newCacheWrapper()
	assert.NoError(t, err)
	defer cache.Close()
	keyName := "first2"
	assert.NoError(t, cache.Get(keyName, 10*time.Second))
	cache.Get(keyName, 2*time.Second)
	time.Sleep(2 * time.Second)
	cache.Get(keyName, 2*time.Second)
	time.Sleep(2 * time.Second)
	cache.Get(keyName, 2*time.Second)
}
