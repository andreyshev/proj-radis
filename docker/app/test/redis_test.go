package test

import (
	"context"
	"strconv"
	"testing"
	"time"

	redis "github.com/redis/go-redis/v9"
)

func TestRedis(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "password",
		DB:       0,
	})
	defer rdb.Close()
	for j := 0; j < 10; j++ {
		for i := 0; i < 1000; i++ {
			if err := rdb.Set(context.TODO(), "key3"+strconv.Itoa(j)+strconv.Itoa(i), time.Now().Nanosecond(), 0).Err(); err != nil {
				t.Error(err)
			}
		}

		time.Sleep(1 * time.Second)
	}
}
