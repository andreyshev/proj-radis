package service

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type app struct {
	cache *cacheWrapper
}

func NewApp() (*app, error) {
	cache, err := newCacheWrapper()
	if err != nil {
		return nil, err
	}
	return &app{
		cache: cache,
	}, nil
}

var defaultTtl = 100 * time.Second

func (a *app) Load(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	key := params["key"]
	//fmt.Println(key)
	if err := a.cache.Get(key, defaultTtl); err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(key))
}
